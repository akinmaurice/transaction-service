const production = {
    PORT: process.env.TRANSACTION_SERVICE_PROD_PORT,
    TRANSACTION_SERVICE_DATABASE_URL: process.env.TRANSACTION_SERVICE_PROD_DATABASE_URL,
    RABBIT_MQ_URL: process.env.RABBIT_MQ_PROD_URL
};

export default production;
