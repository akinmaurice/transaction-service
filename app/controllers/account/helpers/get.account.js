import async from 'async';
import Q from 'q';
import db from '../../../utils/database';
import config from '../../../../config';
import query from '../../../queries/account.queries';


const getUserAccounts = (user_id, callback) => {
    db.any(query.getUserAccounts, [ user_id ])
        .then((accounts) => callback(null, accounts))
        .catch((e) => {
            logger.error('Get-User-Account-Balance-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


function getAccount(user_id) {
    const defer = Q.defer();
    async.waterfall([
        async.apply(getUserAccounts, user_id)
    ], (error, response) => {
        if (error) {
            defer.reject(error);
        } else {
            defer.resolve(response);
        }
    });
    return defer.promise;
}


export default getAccount;
