import async from 'async';
import Response from '../../utils/response';
import config from '../../../config';
import db from '../../utils/database';
import query from '../../queries/account.queries';

const ResponseHandler = new Response();


const checkRequest = (req, callback) => {
    const { params, user } = req;
    return callback(null, params, user);
};


const getAccount = (params, user, callback) => {
    const { account_number } = params;
    db.oneOrNone(query.getSingleAccount, [ account_number ])
        .then((account) => {
            if (!account) {
                return callback('could not find that account');
            }
            if (account.user_id !== user.id) {
                return callback('Please provide a valid account number');
            }
            return callback(null, account);
        })
        .catch((e) => {
            console.log('Get-User-Account-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


function getAccountBalance(req, res) {
    async.waterfall([
        async.apply(checkRequest, req),
        getAccount
    ], (error, response) => {
        if (error) {
            res.status(400).json(ResponseHandler.error(
                req.originalUrl,
                error,
                'Bad Request'
            ));
        } else {
            res.status(200).json(ResponseHandler.success(req.originalUrl, {
                message: 'successfully fetched account',
                account_data: response
            }));
        }
    });
}


export default getAccountBalance;
