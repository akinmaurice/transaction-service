import async from 'async';
import Response from '../../utils/response';
import config from '../../../config';
import getAccount from './helpers/get.account';

const ResponseHandler = new Response();


const getUserAccount = (req, callback) => {
    const { user } = req;
    const { id } = user;
    getAccount(id)
        .then((accounts) => callback(null, accounts))
        .catch((e) => {
            logger.error('Get-User-Account-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


function getUserAccounts(req, res) {
    async.waterfall([
        async.apply(getUserAccount, req)
    ], (error, response) => {
        if (error) {
            res.status(400).json(ResponseHandler.error(
                req.originalUrl,
                error,
                'Bad Request'
            ));
        } else {
            res.status(200).json(ResponseHandler.success(req.originalUrl, {
                message: 'successfully fetched user accounts',
                account_data: response
            }));
        }
    });
}


export default getUserAccounts;
