import async from 'async';
import Response from '../../utils/response';
import checkRequestBody from '../../utils/request.body.verifier';
import config from '../../../config';
import getAccount from './helpers/get.account';

const ResponseHandler = new Response();


const checkRequest = (req, callback) => {
    const error = checkRequestBody(req.body, [
        'user_id'
    ]);
    if (error) {
        return callback(error);
    }
    const { body } = req;
    return callback(null, body);
};


const getUserAccount = (body, callback) => {
    const { user_id } = body;
    getAccount(user_id)
        .then((accounts) => callback(null, accounts))
        .catch((e) => {
            logger.error('Get-User-Account-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


function getClientAccounts(req, res) {
    async.waterfall([
        async.apply(checkRequest, req),
        getUserAccount
    ], (error, response) => {
        if (error) {
            res.status(400).json(ResponseHandler.error(
                req.originalUrl,
                error,
                'Bad Request'
            ));
        } else {
            res.status(200).json(ResponseHandler.success(req.originalUrl, {
                message: 'successfully fetched client accounts',
                account_data: response
            }));
        }
    });
}


export default getClientAccounts;
