import async from 'async';
import Q from 'q';
import moment from 'moment';
import db from '../../utils/database';
import config from '../../../config';
import query from '../../queries/account.queries';


const addAccount = (user, callback) => {
    const { id, first_name, last_name } = user;
    db.one(
        query.createAccount,
        [
            id,
            'SAVING',
            `${first_name} ${last_name}`,
            moment(),
            moment()
        ]
    )
        .then((account) => callback(null, account))
        .catch((e) => {
            logger.error('Add-User-Account-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


const addAccountDetails = (account, callback) => {
    const { account_number } = account;
    db.none(
        query.createAccountDetails,
        [
            account_number,
            0.00,
            0.00,
            moment(),
            moment()
        ]
    )
        .then(() => callback(null, 'done'))
        .catch((e) => {
            logger.error('Add-User-Account-Detail-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


function setupAccount(user) {
    const defer = Q.defer();
    async.waterfall([
        async.apply(addAccount, user),
        addAccountDetails
    ], (error, response) => {
        if (error) {
            defer.reject(error);
        } else {
            defer.resolve(response);
        }
    });
    return defer.promise;
}


export default setupAccount;
