import async from 'async';
import Q from 'q';
import moment from 'moment';
import db from '../../../utils/database';
import config from '../../../../config';
import query from '../../../queries/deposit.queries';


const roundNumber = (value, decimals) => Number(`${Math.abs(Math.round(`${value}e${decimals}`))}e-${decimals}`);


const createTransactionDetails = (data, callback) => {
    const { user_id } = data;
    db.one(
        query.createTransactionDetails,
        [
            user_id,
            moment(),
            moment()
        ]
    )
        .then((transaction) => callback(null, data, transaction))
        .catch((e) => {
            logger.error('Create-Transaction-Detail-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


const createAccountTransaction = (data, transaction, callback) => {
    const { account_number } = data;
    const { id } = transaction;
    let { amount } = data;
    amount = roundNumber(amount, 2);
    db.none(
        query.createAccountTransaction,
        [
            id,
            account_number,
            'CLIENT_DEPOSIT',
            amount,
            0.00,
            moment()
        ]
    )
        .then(() => callback(null, 'Done'))
        .catch((e) => {
            logger.error('Create-Account-Transaction-Error', e, {
                serviceName: config.serviceName
            });
            return callback('Unknown Error');
        });
};


function deposit(data) {
    const defer = Q.defer();
    async.waterfall([
        async.apply(createTransactionDetails, data),
        createAccountTransaction
    ], (error, response) => {
        if (error) {
            defer.reject(error);
        } else {
            defer.resolve(response);
        }
    });
    return defer.promise;
}


export default deposit;
