import onBoardUserConsumer from './consumers/on.board.user.consumer';


const newUserQueue = (config) => {
    onBoardUserConsumer(config.messageQueue.onBoardUser);
};


const messageQueues = (config) => {
    newUserQueue(config);
};

export default messageQueues;
