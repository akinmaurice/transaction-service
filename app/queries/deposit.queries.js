const queries = {
    createTransactionDetails: `
        INSERT INTO transaction_details(user_id, created_at, updated_at)
        VALUES($1, $2, $3) RETURNING id
    `,
    createAccountTransaction: `
        INSERT INTO account_transaction(transaction_details_id,
            account_number, transaction_type, credit, debit, executed_at)
        VALUES($1, $2, $3, $4, $5, $6)
    `
};

export default queries;
