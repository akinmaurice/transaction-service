const queries = {
    getUserAccounts: `
        SELECT
            user_account.account_type,
            user_account.account_number,
            user_account.account_name,
            account.account_balance::float,
            account.book_balance::float
        FROM
            user_account
        LEFT JOIN
            account
        ON
            user_account.account_number = account.account_number
        WHERE
            user_account.user_id = $1
    `,
    createAccount: `
        INSERT INTO user_account(user_id, account_type, account_name, created_at, updated_at)
        VALUES($1, $2, $3, $4, $5) RETURNING account_number
    `,
    createAccountDetails: `
        INSERT INTO account(account_number, account_balance, book_balance, created_at, updated_at)
        VALUES($1, $2, $3, $4, $5)
    `,
    getSingleAccount: `
        SELECT
            user_account.account_type,
            user_account.user_id,
            user_account.account_number,
            user_account.account_name,
            account.account_balance::float,
            account.book_balance::float
        FROM
            user_account
        LEFT JOIN
            account
        ON
            user_account.account_number = account.account_number
        WHERE
            user_account.account_number = $1
    `
};

export default queries;
