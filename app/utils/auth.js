import _ from 'lodash';

const extractUser = (req, res, next) => {
    if (req.headers && req.headers.gatewayauth) {
        const decrypted_auth = Buffer.from(req.headers.gatewayauth, 'base64').toString('ascii');
        const parsedUser = JSON.parse(decrypted_auth);
        req.user = parsedUser;
        return next();
    }
    return res.status(403).json({
        message: 'User is not authorized'
    });
};

const hasAuthorization = (role) => (req, res, next) => {
    if (_.intersection(req.user.roles, role).length) {
        return next();
    }
    return res.status(403).json({
        message: 'User is not authorized to perform the role'
    });
    next();
};


export { hasAuthorization, extractUser };
