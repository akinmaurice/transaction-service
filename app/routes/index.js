import express from 'express';

import * as auth from '../utils/auth';
import getUserAccounts from '../controllers/account/get.user.account';
import getClientAccounts from '../controllers/account/get.client.account';
import getAccountBalance from '../controllers/account/get.account.balance';

const router = express.Router();

router.get(
    '/',
    (req, res) => {
        res.status(200).json({
            message: 'Transaction Service'
        });
    }
);


router.get(
    '/account',
    auth.extractUser,
    getUserAccounts
);


router.get(
    '/account/:account_number',
    auth.extractUser,
    getAccountBalance
);


router.post(
    '/account/client',
    auth.extractUser,
    getClientAccounts
);

export default router;
