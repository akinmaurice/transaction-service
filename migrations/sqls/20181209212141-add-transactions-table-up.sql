/* Replace with your SQL commands */
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE account_type(
    id VARCHAR PRIMARY KEY DEFAULT 'acc-type-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    account_type VARCHAR(50) UNIQUE NOT NULL,
    description VARCHAR(200),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE TABLE transaction_type(
    id VARCHAR PRIMARY KEY DEFAULT 'trans-type-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    transaction_type VARCHAR(50) UNIQUE NOT NULL,
    description VARCHAR(200),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE SEQUENCE account_number_sequence
 START 1010101010
 INCREMENT 1;

CREATE TABLE user_account(
    id VARCHAR PRIMARY KEY DEFAULT 'user-acc-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    user_id  VARCHAR(50) NOT NULL,
    account_type VARCHAR(50) REFERENCES account_type(account_type) ON UPDATE CASCADE,
    account_number INTEGER UNIQUE NOT NULL DEFAULT nextval('account_number_sequence'),
    account_name VARCHAR(50),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE TABLE account(
    id VARCHAR PRIMARY KEY DEFAULT 'acc-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    account_number INTEGER REFERENCES user_account(account_number) ON UPDATE CASCADE,
    account_balance NUMERIC(20, 6),
    book_balance NUMERIC(20, 6),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE TABLE transaction_details(
  id VARCHAR PRIMARY KEY DEFAULT 'trans-detail-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    user_id VARCHAR(50),
    created_at TIMESTAMPTZ NOT NULL,
    updated_at TIMESTAMPTZ
);


CREATE TABLE account_transaction(
    id VARCHAR PRIMARY KEY DEFAULT 'transact-' || LOWER(
            REPLACE(
                CAST(uuid_generate_v1mc() As varchar(50))
                , '-','')
            ),
    transaction_details_id VARCHAR(50) NOT NULL REFERENCES transaction_details(id) ON UPDATE CASCADE,
    account_number INTEGER REFERENCES user_account(account_number) ON UPDATE CASCADE,
    transaction_type VARCHAR(50) REFERENCES transaction_type(transaction_type) ON UPDATE CASCADE,
    credit NUMERIC(20, 6) NOT NULL,
    debit NUMERIC(20, 6) NOT NULL,
    executed_at TIMESTAMPTZ NOT NULL
);


INSERT INTO account_type(account_type, description, created_at, updated_at)
VALUES('SAVING', 'Client Saving Account', NOW(), NOW());

INSERT INTO account_type(account_type, description, created_at, updated_at)
VALUES('CURRENT', 'Client Current Account', NOW(), NOW());


INSERT INTO transaction_type(transaction_type, description, created_at, updated_at)
VALUES('CLIENT_DEPOSIT', 'Client Direct Deposit', NOW(), NOW());


INSERT INTO transaction_type(transaction_type, description, created_at, updated_at)
VALUES('CLIENT_WITHDRAWAL', 'Client Withdrawal', NOW(), NOW());


INSERT INTO transaction_type(transaction_type, description, created_at, updated_at)
VALUES('CLIENT_TRANSFER', 'Transfer from one client to another', NOW(), NOW());
