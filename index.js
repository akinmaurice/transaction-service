import express from 'express';
import config from './config';
import messageQueue from './app/services/MQ/index';

// Bootstrap express
import expressConfig from './config/express';

const port = process.env.PORT || 3073;
const app = express();


messageQueue(config);
expressConfig(app);

app.listen(port);
logger.info(`Transaction Service started on port ${port}`);

export default app;
