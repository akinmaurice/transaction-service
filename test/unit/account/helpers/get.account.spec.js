import chai from 'chai';
import sinon from 'sinon';
import rewire from 'rewire';

import db from '../../../../app/utils/database';


const getAccount = rewire('../../../../app/controllers/account/helpers/get.account.js');

let sandbox;
const { expect } = chai;


describe('It validates Helper Method to Get All User Account', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Gets User Accounts', () => {
        const accounts = [ {
            account_number: '123313228',
            account_balance: 0,
            book_balance: 0,
            account_name: 'Test User',
            account_type: 'SAVING'
        } ];
        sandbox.stub(db, 'any').returns(Promise.resolve(accounts));
        const user_id = 'user-123456';
        const getUserAccounts = getAccount.__get__('getUserAccounts');
        getUserAccounts(user_id, (error, response) => {
            expect(error).to.equal(null);
            expect(response).to.equal(accounts);
            expect(response).to.have.property('account_number');
            expect(response).to.have.property('account_balance');
            expect(response).to.have.property('book_balance');
            expect(response).to.have.property('account_type');
        });
    });
});
