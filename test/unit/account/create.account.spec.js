import chai from 'chai';
import sinon from 'sinon';
import rewire from 'rewire';

import db from '../../../app/utils/database';


const setupAccount = rewire('../../../app/controllers/account/create.account');

let sandbox;
const { expect } = chai;


describe('It validates all Methods to Create user Account', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Creates User Account', () => {
        const account = {
            account_number: '123313228'
        };
        sandbox.stub(db, 'one').returns(Promise.resolve(account));
        const reqBody = {
            id: 'user-12345',
            first_name: 'Test',
            last_name: 'User'
        };
        const addAccount = setupAccount.__get__('addAccount');
        addAccount(reqBody, (error, response) => {
            expect(error).to.equal(null);
            expect(response).to.equal(account);
            expect(response).to.have.property('account_number');
        });
    });


    it('Creates User Account Details', () => {
        sandbox.stub(db, 'none').returns(Promise.resolve());
        const reqBody = {
            account_number: '123313228'
        };
        const addAccountDetails = setupAccount.__get__('addAccountDetails');
        addAccountDetails(reqBody, (error, response) => {
            expect(error).to.equal(null);
            expect(response).to.equal('done');
        });
    });
});
