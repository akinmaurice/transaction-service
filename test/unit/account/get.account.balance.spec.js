import chai from 'chai';
import sinon from 'sinon';
import assert from 'assert';
import rewire from 'rewire';

import db from '../../../app/utils/database';


const getAccountBalance = rewire('../../../app/controllers/account/get.account.balance.js');

let sandbox;
const { expect } = chai;


describe('It validates all Methods to Get Logged In User Single Account Balance', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Get User Single Account', () => {
        const account = {
            account_number: '123313228',
            user_id: 'user-12345',
            account_balance: 0,
            book_balance: 0,
            account_name: 'Test User',
            account_type: 'SAVING'
        };
        sandbox.stub(db, 'oneOrNone').returns(Promise.resolve(account));
        const user = {
            id: 'user-12345',
            first_name: 'Test',
            last_name: 'User'
        };
        const params = {
            account_number: '123313228'
        };
        const getAccount = getAccountBalance.__get__('getAccount');
        getAccount(params, user, (error, response) => {
            expect(error).to.equal(null);
            expect(response).to.equal(account);
            expect(response).to.have.property('account_number');
            expect(response).to.have.property('user_id');
            expect(response).to.have.property('account_balance');
            expect(response).to.have.property('book_balance');
            expect(response).to.have.property('account_type');
        });
    });
});
